# translation of bulmafact_ca_ES.po to
# Header entry was created by KBabel!
#
# Fco. Javier M. C. <fcojavmc@gmail.com>, 2009.
msgid ""
msgstr ""
"Report-Msgid-Bugs-To: info@iglues.org\n"
"POT-Creation-Date: 2011-08-16 13:16+0200\n"
"PO-Revision-Date: 2011-03-09 16:45+0100\n"
"Last-Translator: Walter Garcia-Fontes <walter.garcia@upf.edu>\n"
"Language-Team:  Catalan\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ui_bgbulmagesbase.h:418
msgid "&Cancelar"
msgstr "&Cancel·la"

#: bgbulmages.cpp:103
msgid "&Salir"
msgstr "&Surt"

#: ui_bgbulmagesbase.h:387
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt;\">Programa de </span><span style=\" font-size:10pt; font-"
"weight:600;\">contabilidad</span><span style=\" font-size:10pt;\">. Adaptado "
"al plan contable Español.</span></p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\"font-"
"size:10pt;\">Programa de </span><span style=\" font-size:10pt; font-"
"weight:600;\">comptabilitat</span><span style=\"font-size:10pt;\">. Adaptat "
"al Pla Comptable Espanyool.</span></p></body></html>"

#: ui_bgbulmagesbase.h:395
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt;\">Programa para emitir </span><span style=\" font-size:10pt; font-"
"weight:600;\">facturas</span><span style=\" font-size:10pt;\">. Lleva un "
"control de clientes y proveedores, además de todos los documentos "
"relacionados con el comercio.</span></p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px;margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\"font-"
"size:10pt;\">Programa per emetre </span><span style=\"font-size:10pt; font-"
"weight:600;\">factures</span><span style=\"font-size:10pt;\">. Porta un "
"control de clients i proveïdors, a més de tots els documents relacionats amb "
"el comerç.</span></p></body></html>"

#: ui_bgbulmagesbase.h:403
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt;\">Programa para la gestión de un Terminal Punto de Venta </"
"span><span style=\" font-size:10pt; font-weight:600;\">(TPV).</span><span "
"style=\" font-size:10pt;\"> Admite la impresión de tickets y lector de "
"código de barras.</span></p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:14pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt;\">Programa per a la gestió d'un Terminal Punt de Venda </"
"span><span style=\" font-size:10pt; font-weight:600;\">(TPV).</span><span "
"style=\" font-size:10pt;\"> Admet la impressió de tickets i lector de codi "
"de barres.</span></p></body></html>"

#: ui_bgbulmagesbase.h:411
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:7pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt; font-weight:600;\">Configuración</span><span style=\" font-"
"size:10pt;\"> general del conjunto de aplicaciones </span><span style=\" "
"font-size:10pt; font-weight:600;\">BulmaGes</span><span style=\" font-"
"size:10pt;\">. Administra la base de datos, permisos de usuarios y altas/"
"bajas de funcionalidades del resto de programas mediante el uso de 'plugins'."
"</span></p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:7pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"size:10pt; font-weight:600;\">Configuració</span><span style=\" font-"
"size:10pt;\"> general del conjunt d'aplicaciones </span><span style=\" font-"
"size:10pt; font-weight:600;\">BulmaGes</span><span style=\" font-size:10pt;"
"\">. Administra la base de dades, permissos de usuaris i altes/baixes de "
"funcionalitats de la resta de programes mitjançant l'ús de 'plugins'.</"
"span></p></body></html>"

#: main.cxx:67
msgid "Bandeja"
msgstr "Safata"

#: ui_bgbulmagesbase.h:385 bgbulmages.cpp:95
msgid "BulmaCont"
msgstr "BulmaCont"

#: ui_bgbulmagesbase.h:393 bgbulmages.cpp:97
msgid "BulmaFact"
msgstr "BulmaFact"

#: ui_bgbulmagesbase.h:409 bgbulmages.cpp:101
msgid "BulmaSetup"
msgstr "BulmaSetup"

#: ui_bgbulmagesbase.h:401 bgbulmages.cpp:99
msgid "BulmaTPV"
msgstr "BulmaTPV"

#: ui_bgbulmagesbase.h:384
msgid "Dialog"
msgstr "Dialog"

#: ui_bgbulmagesbase.h:417
msgid "Lanzador"
msgstr "Llençador"

#: bgbulmages.cpp:33
msgid "Lanzador BulmaGes"
msgstr "Llençador BulmaGes"

#: ui_bgbulmagesbase.h:400
msgid "Lanzar Bulma&Fact"
msgstr "Llençador Bulma&Fact"

#: ui_bgbulmagesbase.h:416
msgid "Lanzar Bulma&Setup"
msgstr "Llençador Bulma&Setup"

#: ui_bgbulmagesbase.h:408
msgid "Lanzar Bulma&TPV"
msgstr "Llençador Bumla&TPV"

#: ui_bgbulmagesbase.h:392
msgid "Lanzar BulmaC&ont"
msgstr "Llençador BulmaC&ont"

#: main.cxx:68
msgid "No se detecta ninguna bandeja en el sistema"
msgstr "No es detecta cap safata al sistema"

#: bgbulmages.cpp:165
msgid "No se encuentra kdesudo"
msgstr ""
